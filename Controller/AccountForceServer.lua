
---------------------------------------------------------------------
------------- Contrôleur AutoReconnect sur le bon serveur -----------
------------- Créateur : Thinkanf -----------------------------------

-------- CONST --------
delayCheck = 30000 -- Délai entre deux check de serveur 60000 = 1 minute
serverName = "Draconiros" -- Nom du serveur où l'on souhaite la connexion Auto
ankabotKey = ""
serverToCheck = {"Draconiros", "Imagiro"}
moderatorToCheck = {"[Dikaion]", "[Nexyroz]"}
timeToCheck = 60;
delayToReconnect = 1000 * 30; 


function move()
    while true do
        global:printMessage("Début de la vérification des serveurs....")
        
        if checkModerator() then
            disconnectAll()
            global:printMessage("Un modérateur est présent ou a été présent lors des " .. timeToCheck .. " minutes. Les comptes restent déconnecté pendant " .. delayToReconnect)
            global:delay(delayToReconnect);
        end

        checkAccount()
        runScript()
        global:delay(delayCheck);
    end
end

function checkAccount()
    for _, account in pairs(ankabotController:getLoadedAccounts()) do
        -- On vérifie que le compte courrant n'est pas le compte contrôleur
        if account.isController() == false then
            global:printMessage("Vérification du compte : " .. account.getUsername())
            -- Verification du serveur 
            if account.isAccountConnected() == true then
                currentServer = account.character.server()
                global:printMessage("Le personnage du compte " .. account.getUsername() .. " est actuellement connecté sur le serveur " .. currentServer)

                if currentServer != serverName then
                    global:printMessage("Le personnage du compte " .. account.getUsername() .. " n'est pas sur le bon serveur...")

                    -- Deconnexion et choix du bon servveur
                    account.disconnect()
                    account.forceServer(serverName)
                end
            end

            -- Reconnexion du compte
            if account.isAccountConnected() == false then
                account.connect()
            end
        end
    end
end

function runScript()
    for _, account in pairs(ankabotController:getLoadedAccounts()) do
        if account.isAccountFullyConnected() == true then
            -- Si le script n'est pas en cours d'éxécution, on le start
            if account.isScriptPlaying() == false then
                global:printMessage("Lancement du script sur le compte " .. account.getUsername())
                account.startScript()
            end
        end
    end
end




function isInTable(tableOrList, value)
	for i = 1, #tableOrList do
		if value == tableOrList[i] then
			return true
		end
	end
	return false
end


function checkModerator()
	for i = 1, #server do
		local APImoderateur = developer:getRequest("https://www.ankabot.dev/API/Moderator.php?key="..ankabotKey.."&serveur="..serverToCheck[i].."&last="..timeToCheck)
		if isInTable(moderatorToCheck, APImoderateur) then
			return true
		end
	end

	return false
end

function disconnectAll()
    for _, account in pairs(ankabotController:getLoadedAccounts()) do
        account.disconnect()
    end
end